<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;
class Order extends Model
{
    //
    public function user(){
        return $this->belongsTo(User::class);
    }

    public function dishes(){
        return $this->belongsToMany(Dish::class);
    }

    public function detailDishes(){
        return $this->belongsToMany(Dish::class)->withPivot('qty','total_price');
    }


}
