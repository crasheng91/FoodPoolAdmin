<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Dish;
class Kitchen extends Model
{
    //

    public function dishes(){
        return $this->hasMany(Dish::class);
    }
}
