<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function login(Request $request)
    {
            $this->validateLogin($request);

            if ($this->attemptLogin($request)) {
                $user = $this->guard()->user();

                //if is a mobile user
                if (!$user->admin){

                    //return to mobile app
                    $user->generateToken();
                    $user->admin = false;

                    return response()->json([
                        'data' => $user->toArray(),
                    ]);
                }else{
                    //redirect ot home if coming from web dashboard
                    return redirect('/home');
                }

            }
            //if none of above fail the page
            return $this->sendFailedLoginResponse($request);

    }


    public function logout(Request $request)
    {
//        $user->admin;
        $user = Auth::guard('api')->user();
        //it will get null if user not admin so logout the dashboad
        if ($user) {
            $user->api_token = null;
            $user->save();
            return response()->json(['data' => 'User logged out.'], 200);
        }else{
            $this->guard()->logout();
            $request->session()->invalidate();
            return redirect('/');
        }


    }
}
