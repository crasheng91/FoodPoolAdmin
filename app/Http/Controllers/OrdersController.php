<?php

namespace App\Http\Controllers;

use App\Order;
use Illuminate\Contracts\Logging\Log;
use Illuminate\Http\Request;

class OrdersController extends Controller
{
    public function viewOrders(){

        $orders = Order::all();
        return view('orders', compact('orders'));
    }


    public function update(Request $request){
        if ($request->id != ""){
            $order = Order::find($request->id);
            $order->status = ($request->status) == "false" ? false: true;
            $order->save();
            return $order;
        }
    }
}
