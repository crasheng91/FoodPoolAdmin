<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\UploadedFile;
use App\Kitchen;
use Illuminate\Support\Facades\Log;
class RestaurantController extends Controller
{
    //
    public function create(Request $request)
    {

        //Check if valid or not
            //valid then
            //save in disk with time and random key
            //save the title and the path to data base
        // not valid then
            //do feedback 500 status

            $titleRestaurant = $request->restaurantTitle;
            error_log($titleRestaurant, 4);
            $filename = "";
            if ($request->file('restaurantPhoto')->isValid()){
                $filename = time().'.'.$request->file('restaurantPhoto')->getClientOriginalName();
                $request->restaurantPhoto->move(public_path('images'), $filename);
            }
            $kitchen = new Kitchen();
            $kitchen->title = $titleRestaurant;
            $kitchen->picture = $filename;
            $kitchen->active = true;
            $kitchen->save();

            $result = array('status' => "Done");

            $resultJson =  json_encode($result);;
        return $resultJson;
    }


    public function delete(Request $request){
        $kitchen = Kitchen::find($request->getContent());
        $deleted = $kitchen->delete();
        return $deleted?  self::deleteImageFromServer($kitchen->picture): "Something went bad in DB";
    }


    public function update(Request $request){
        if ($request->id != ""){
            $kitchen = Kitchen::findOrFail($request->id);
            Log::info($kitchen);
            Log::info(($request->state) == "false" ? "false": "true");
            $kitchen->active = ($request->state) == "false" ? false: true;
            $kitchen->save();
        }
    }

    private function deleteImageFromServer($filename){
//        $image_path = public_path('images')."/".$filename;  // Value is not URL but directory file path
        $image_path = app_path().'/images/'.$filename;
        error_log($image_path);
        if(File::exists($image_path)) {
//            error_log(unlink($image_path));
            File::delete($image_path);
            return "File deleted successfully";
        }
        return "File not on server bad record";
    }



}
