<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="_token" content="{!! csrf_token() !!}" />

    <title>{{ config('app.name', 'FoodPool') }}</title>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/mine.css') }}" rel="stylesheet">
    <link href="{{ asset('css/sidebar_style.css') }}" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-switch/3.3.4/css/bootstrap3/bootstrap-switch.min.css" rel="stylesheet">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous"></head>
<body>
    <div id="app">
        <nav class="navbar navbar-default navbar-static-top">
            <div class="container">
                <div class="navbar-header">

                    <!-- Collapsed Hamburger -->
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse" aria-expanded="false">
                        <span class="sr-only">Toggle Navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                    <!-- Branding Image -->
                    <a class="navbar-brand" href="{{ url('/') }}">
                        {{ config('app.name', 'FoodPool') }}
                    </a>
                </div>

                <div class="collapse navbar-collapse" id="app-navbar-collapse">
                    <!-- Left Side Of Navbar -->
                    <ul class="nav navbar-nav">
                        &nbsp;
                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="nav navbar-nav navbar-right">
                        <!-- Authentication Links -->
                        @guest
                            <li><a href="{{ route('login') }}">Login</a></li>
                            <li><a href="{{ route('register') }}">Register</a></li>
                        @else
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false" aria-haspopup="true">
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <ul class="dropdown-menu">
                                    <li>
                                        <a href="{{ route('logout') }}"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            Logout
                                        </a>

                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                    </li>
                                </ul>
                            </li>
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>

        @yield('content')
        {{--{!! csrf_field() !!}--}}
    </div>
    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-switch/3.3.4/js/bootstrap-switch.min.js"></script>
    <script src="http://malsup.github.com/jquery.form.js"></script>
    <script>
        $(document).ready(function(){

            //
            $("[name='rest-checkbox']").bootstrapSwitch();

            //For dishes
            $("[name='dish-checkbox']").bootstrapSwitch();

            //order
            $("[name='order-checkbox']").bootstrapSwitch();

            $.ajaxSetup({
                headers: { 'X-CSRF-Token' : $('meta[name="_token"]').attr('content') }
            });

            $('input[name="dish-checkbox"]').on('switchChange.bootstrapSwitch', function(event, state) {
                console.log(state); // true | false
                var id = event.target.getAttribute("data-mine");
                $.post('../../update-dish-status',
                    {'status': state, 'id':id},
                    function(data){
                        console.log(data);
                    });

            });

            $('input[name="order-checkbox"]').on('switchChange.bootstrapSwitch', function(event, state) {
                console.log(state); // true | false
                var id = event.target.getAttribute("data-mine");
                $.post('update-order-status',
                    {'status': state, 'id':id},
                    function(data){
                        console.log(data);
                    });

            });


            $('input[name="rest-checkbox"]').on('switchChange.bootstrapSwitch', function(event, state) {
                console.log(state); // true | false
                // var id = $(this).data('idOfRest');
                var id = event.target.getAttribute("data-mine");
                $.post('update-restaurant',
                    {'state': state, 'id':id},
                    function(data){
                        console.log(data);
                    });
            });


            $(".deleteClicked").click(function (event) {
                event.preventDefault();
                var id = $(this).data("deleteid");
                var agree = confirm("Sure to delete " + id);
                if(agree){
                    console.log("delete")
                    $.ajax({
                        url: "{{url('delete-restaurant')}}", // point to server-side PHP script
                        data: id,
                        type: 'POST',
                        contentType: false, // The content type used when sending data to the server.
                        cache: false, // To unable request pages to be cached
                        processData: false,
                        success: function(data) {
                            console.log(data)
                        }
                    });
                }else{
                    console.log("keep it")
                }
            });

            // $deleted
            $(".deleteDishClicked").click(function (event) {
                event.preventDefault();
                var id = $(this).data("deleteid");
                var agree = confirm("Sure to delete " + id);
                if(agree){
                    $.post('../../delete-dish',
                        {'id':id},
                        function(data){
                            console.log(data);
                            location.reload();
                        });
                }else{
                    console.log("keep it")
                }
            });


            $('.editClicked').each(function () {
               $(this).click(function (event) {
                   $('#myModalLabel').text("Edit Restaurant");
                   $('#closeEditID').show(200);
                   $('#saveEditID').show(200);
                   $('#addNewRestID').hide();
                   console.log(event);
                   console.log(this);
               });
            });

            $('#plusButtonID').click(function (event) {
                    $('#myModalLabel').text("New Restaurant");
                    $('#closeEditID').hide(200);
                    $('#saveEditID').hide(200);
                    $('#addNewRestID').show(200);
                    console.log(event);
                    console.log(this);
            });

            $('#addNewRestID').click(function (event) {
                var extension = $('#restPhotoId').val().split('.').pop().toLowerCase();
                if ($.inArray(extension, ['png', 'jpg']) == -1){
                    $('#errormessage').html('Please Select Valid File... ');
                }else{
                    var restaurantPhoto = $('#restPhotoId').prop('files')[0];
                    var restaurantTitle = $('#restNameId').val();
                    var form_data = new FormData();
                    form_data.append('restaurantPhoto', restaurantPhoto);
                    form_data.append('restaurantTitle', restaurantTitle);

                    $.ajax({
                        async:true,
                        url: "{{url('restaurant')}}", // point to server-side PHP script
                        data: form_data,
                        type: 'POST',
                        dataType: 'json',
                        contentType: false, // The content type used when sending data to the server.
                        cache: false, // To unable request pages to be cached
                        processData: false,
                        success: function() {
                            console.log("Done")
                            // $("#refreshTable").load(location.href + " #refreshTable");
                            // $("[name='my-checkbox']").bootstrapSwitch();
                            location.reload();
                        },
                        error: function(error){
                            console.log(error);
                        }
                    });
                }
            });

            $('#addNewDishId').click(function (event) {
                event.preventDefault();
                var extension = $('#dishPhotoId').val().split('.').pop().toLowerCase();
                var kitchen_id = $(this).data('kitchen_id');
                if ($.inArray(extension, ['png', 'jpg']) == -1){
                    $('#errormessage').html('Please Select Valid File... ');
                }else{
                    var dishPhoto = $('#dishPhotoId').prop('files')[0];
                    var dishTitle = $('#dishNameId').val();
                    var dishPrice = $('#dishPriceId').val();
                    console.log(dishPrice)
                    var form_data = new FormData();
                    form_data.append('dishPhoto', dishPhoto);
                    form_data.append('dishName', dishTitle);
                    form_data.append('kitchen_id', kitchen_id);
                    form_data.append('kitchen_price', dishPrice);

                    if (dishPrice < 0)
                        return;

                    console.log(form_data.entries());

                    $.ajax({
                        async:true,
                        url: "{{url('new-dish')}}", // point to server-side PHP script
                        data: form_data,
                        type: 'POST',
                        dataType: 'json',
                        contentType: false, // The content type used when sending data to the server.
                        cache: false, // To unable request pages to be cached
                        processData: false,
                        success: function() {
                            console.log("Done")
                            // $("#refreshTable").load(location.href + " #refreshTable");
                            // $("[name='my-checkbox']").bootstrapSwitch();
                            location.reload();
                        },
                        error: function(error){
                            console.log(error);
                        }
                    });
                }
            });
        });

    </script>
</body>
</html>
