@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div id="refreshTable">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">Dishes<a href="#" class="pull-right" id="plusButtonID" data-toggle="modal" data-target="#addDishModal">
                                    <i class="fa fa-plus" aria-hidden="true"></i></a></h3>
                        </div>

                        <div class="panel-body">
                            All the Available dishes for this resturant on the dashboard
                            {{--<div class="pull-left form-control-feedback" id="errormessage"></div>--}}

                        </div>

                        <table id="mytable" class="table table-bordred table-striped">

                            <thead>
                            <th style="text-align: center; vertical-align: middle;">Dish Title</th>
                            <th style="padding-left: 10px;">Picture</th>
                            <th style="padding-left: 10px;">Price</th>
                            <th style="padding-left: 10px;">Kitchen</th>
                            <th style="text-align: center; vertical-align: middle;">Delete</th>
                            <th style="text-align: center; vertical-align: middle;">Availability</th>
                            </thead>
                            <tbody>
                            @foreach($dishes as $dish)
                                <tr>
                                    <td style="text-align: center; vertical-align: middle;"><a href="#">{{$dish->title}}</a></td>
                                    <td style="text-align: center; vertical-align: middle;"><img  style="width:200px;height:100px;" src="/images/{{$dish->picture}}" class="img-responsive"></td>
                                    <td style="text-align: center; vertical-align: middle;">{{$dish->price}}</td>
                                    <td style="text-align: center; vertical-align: middle;">{{$kitchen_name}}</td>
                                    <td style="text-align: center; vertical-align: middle;">
                                        <p data-placement="top" data-toggle="tooltip" title="Delete">
                                            <button class="btn btn-danger btn-xs deleteDishClicked" data-title="Delete" data-deleteid="{{$dish->id}}">
                                                <i class="fa fa-trash" aria-hidden="true"></i></button></p>
                                    </td>
                                    <td style="text-align: center; vertical-align: middle;">
                                        <input type="hidden" id="#dishIdHidden" value="{{$dish->id}}"/>
                                        @if($dish->active == 1)
                                            <input type="checkbox" data-size="mini" data-mine="{{$dish->id}}"  name="dish-checkbox" checked></td>
                                    @else
                                        <input type="checkbox" data-size="mini" data-mine="{{$dish->id}}"  name="dish-checkbox" unchecked></td>
                                    @endif
                                </tr>
                            @endforeach

                            </tbody>

                        </table>

                        <!-- Modal -->
                        <div class="modal fade" id="addDishModal" tabindex="-1" role="dialog" aria-labelledby="addDishModalLabelDish">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                        <h4 class="modal-title" id="addDishModalLabel">New Dish</h4>
                                    </div>
                                    <div class="modal-body">
                                        <form enctype="multipart/form-data" name="my_form" class="form-label-left" id="my_form" action="{{ route('createRestaurant') }}" method="POST">
                                            <div class="form-group">
                                                <label class="col-md-6 control-label">Dish name <span class="required">*</span></label>
                                                <input type="text" name="resTitle" placeholder="Dish Name" id="dishNameId" class="form-control"/>

                                                <br/>
                                                <label class="col-md-6 control-label">Dish name <span class="required">*</span></label>
                                                <input type="number" name="dishPrice" placeholder="Dish price" id="dishPriceId" class="form-control"/>


                                                <br/>
                                                <input type="file" name="dishphoto" id="dishPhotoId" class="form-control"/>
                                            </div>
                                        </form>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="submit" class="btn btn-primary" data-kitchen_id="{{$kitchen_id}} "id="addNewDishId" data-dismiss="modal">Add Dish</button>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>

            </div>

        </div>
    </div>
@endsection

